#!/usr/bin/perl -T

use CGI;
use Cache::FileCache;
use strict;
use XML::LibXML;

my $cache = Cache::FileCache->new(
  { cache_root => '/tmp', namespace => 'FCMB-Faker' }
);

my @status = (
  'Successful', 'Failed', 'Pending', 'Cancelled', 'Not Processed',
  'Invalid Merchant', 'Inactive Merchant', 'Inactive Order ID',
  'Duplicate Order ID', 'Invalid Amount'
);

my $cgi = CGI->new;
my $oid = $cgi->param('ORDER_ID');

# inefficient, but this is not production code, so who cares?
my ($txn) = grep { $_->{orderId} eq $oid }
            map  { $cache->get($_) } $cache->get_keys;
my @out;
if ($txn) {
  @out = (
    MerchantID          => $txn->{mercId},
    OrderID             => $txn->{orderId},
    StatusCode          => $txn->{status},
    Status              => $status[$txn->{status}],
    Amount              => sprintf('%.2f', $txn->{amt}),
    Date                => $txn->{date},
    TransactionRef      => $txn->{reference},
    PaymentRef          => sprintf('%06d', rand(1000000)),
    ResponseCode        => sprintf('%02d', rand(100)),
    ResponseDescription => 'response description',
    CurrencyCode        => $txn->{currCode},
  );
} else {
  @out = ( Status => 'Invalid Order ID', StatusCode => '07' );
}
my $doc = XML::LibXML::Document->new;
my $root = $doc->createElement('UPay');
$doc->setDocumentElement($root);
while (@out) {
  my $name = shift @out;
  my $value = shift @out;
  my $node = $doc->createElement($name);
  $node->appendChild( XML::LibXML::Text->new($value) );
  $root->appendChild($node);
}

my $content = $doc->toString;
print $cgi->header('text/xml');
print $content;

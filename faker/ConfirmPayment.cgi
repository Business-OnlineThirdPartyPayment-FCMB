#!/usr/bin/perl -T

my $LANDING_URL =
  "http://localhost:2080/selfservice.cgi?action=finish_thirdparty_payment";

use CGI;
use URI;
use Date::Format 'time2str';
use Cache::FileCache;
use strict;

my $cache = Cache::FileCache->new(
  { cache_root => '/tmp', namespace => 'FCMB-Faker' }
);

my $landing = URI->new($LANDING_URL);

my $cgi = CGI->new;
my $reference = $cgi->param('reference');
my $txn = $cache->get($reference);

if ( $cgi->param('submit') eq 'Cancel' ) {
  $txn->{status} = 3; #canceled
  $landing->query_form(_cancel => 1);
} else {

  # some information captured from the customer
  # (in Real Life this would also be their credit card/bank account number)
  $txn->{first} = $cgi->param('first');
  $txn->{last}  = $cgi->param('last');
  # set status = the last digit of cents
  # (0 = success)
  my $cents = ($txn->{amt} - int($txn->{amt})) * 100;
  $txn->{status} = $cents % 10;
  $txn->{date} = time2str('%Y-%m-%d', time);

  $landing->query_form(
    $landing->query_form,
    'OrderID' => $txn->{orderId},
    'TransactionReference' => $reference
  );
}
# update the cache
$cache->set($reference => $txn);
print $cgi->redirect($landing);

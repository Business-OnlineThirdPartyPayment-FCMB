#!/usr/bin/perl -T

use CGI;
use Cache::FileCache;
use strict;

my $cache = Cache::FileCache->new(
  { cache_root => '/tmp', namespace => 'FCMB-Faker' }
);
my $cgi = CGI->new;
my %transaction = map { $_ => ($cgi->param($_) || '') }
  qw( mercId currCode amt orderId prod email );

my $reference = sprintf('%06d%04d', $transaction{mercId}, int(rand(10000)));
$transaction{reference} = $reference;
$transaction{status} = 2; #pending

$cache->set($reference, \%transaction);

my $content = qq!
<HTML>
  <HEAD><TITLE>Not FCMB Web Payment</TITLE></HEAD>
  <BODY><H3>Confirm your payment</H3>
    <FORM METHOD="POST" ACTION="ConfirmPayment.cgi">
      <TABLE CELLSPACING=0 STYLE="border: 1px solid">
        <TR><TD>Order #</TD><TD>!.$transaction{orderId}.qq!</TD></TR>
        <TR><TD>Product</TD><TD>!.$transaction{prod}.qq!</TD></TR>
        <TR><TD>Amount </TD><TD>!.$transaction{amt}.qq!</TD></TR>
        <TR><TD>First Name</TD><TD><INPUT NAME="first"></TD></TR>
        <TR><TD>Last Name</TD><TD><INPUT NAME="last"></TD></TR>
      </TABLE><BR>
      <INPUT TYPE="hidden" name="reference" value="!.$reference.qq!">
      <INPUT TYPE="submit" name="submit" value="Pay Now">
      <INPUT TYPE="submit" name="submit" value="Cancel">
    </FORM>
  </BODY>
</HTML>
!;

print $cgi->header('text/html',
                   'Content-Length' => length($content));
print $content;
          
